import { actions } from "../../Store/duck";
import { useAppDispatch } from "../../Store/Config";
import { Button } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { useState } from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";

const FormContainer = styled.li`
  border: 2px solid gray;
  border-radius: 30px;
  align-self: center;
  width: 500px;
  height: 320px;
  display: flex;
  text-align: center;
  flex-direction: column;
  margin-top: 200px;
  padding: 30px;
`

export const SignInForm = () => {

  const navigation = useNavigate();

  const dispatch = useAppDispatch();

  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const trySignIn = async () => {
    await dispatch(actions.auth.signIn({ email, password })).unwrap();
    navigation('/');
  };

  return (
    <FormContainer>
      <h1>Вход</h1>
      <TextField 
        id="outlined-basic" 
        label="Email" 
        variant="outlined" 
        type="text"
        margin="normal" 
        value={email} 
        onChange={(e) => setEmail(e.target.value)}
      />
      <TextField 
        id="outlined-basic" 
        label="Password" 
        variant="outlined" 
        type="password" 
        margin="normal" 
        value={password} 
        onChange={(e) => setPassword(e.target.value)}
      />
      <Button 
        variant="outlined" 
        onClick={trySignIn}
      >
        Войти
      </Button>
    </FormContainer>
  );
}
