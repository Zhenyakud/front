import { Button, IconButton, TextField } from "@material-ui/core";
import { ComponentProps, FC, useEffect, useState } from "react";
import styled from "styled-components";
import { CardResponseDto } from "../../Api/Types/types";
import { Cards } from "../Cards/Cards";
import DeleteIcon from '@material-ui/icons/Delete';
import { actions, useAppDispatch } from "../../Store";

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #dfe3e6;
  border-radius: 3px;
  width: 300px;
  padding: 8px;
  margin: 0px 8px;
  height: 100%;
  gap: 15px;
`;

const ButtonContainer = styled.div`
  display: flex;
`

const AddCardContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;
  margin-left: 8px;
  background-color: white;
  padding: 10px;
  border-radius: 3px;
`

export interface ColumnProps extends ComponentProps<"div"> {
  cards: CardResponseDto[];
  columnId: string;
  title: string;
}

export const Columns: FC<ColumnProps> = ({ columnId, cards, title }) => {

  const [isActive, setIsActive] = useState(false);

  const [text, setText] = useState("");

  const tryCreateCard = async () => {
    await dispatch(actions.cards.createCard({ text, columnId }));
  }

  const trySetIsActiveTrue = () => {
    setIsActive(true)
  };

  const trySetIsActiveFalse = () => {
    setIsActive(false)
  };

  const dispatch = useAppDispatch();

  const tryDeleteColumn = async () => {
    await dispatch(actions.columns.deleteColumn(columnId));
  }

  const renderCards = () => {
    return cards.map((card) => {
      return (
        <Cards
          key={card.id}
          cardId={card.id}
          text={card.text}
          columnId={columnId}
        />
      );
    });
  };

  return (
    <ColumnContainer key={columnId}>
      <h2>{title}</h2>
      {renderCards()}

      {isActive ?
        <AddCardContainer>
          <TextField
            id="outlined-basic"
            label="Название"
            variant="outlined"
            value={text}
            onChange={(e) => setText(e.target.value)}
          />
          <Button
            variant="contained"
            onClick={() => {
              trySetIsActiveFalse();
              tryCreateCard();
            }}
          >
            Добавить карточку
          </Button>
        </AddCardContainer>
        :
        <ButtonContainer>
          <Button
            variant="contained"
            onClick={trySetIsActiveTrue}
          >
            Создать карточку
          </Button>
        </ButtonContainer>
      }

      <ButtonContainer>
        <IconButton aria-label="delete" size="small" onClick={tryDeleteColumn}>
          <DeleteIcon />
        </IconButton>
      </ButtonContainer>
    </ColumnContainer>
  );
}
