import { Link } from "react-router-dom";
import styled from "styled-components";

const NavContainer = styled.div`
  position: fixed;
  width: 100%; 
  display: flex;
  justify-content: flex-end;
  flex-direction: row;
  margin: 0;
  padding: 0;
  background-color: black;
`;

const UlContainer = styled.ul`
  margin-right: 50px;
  margin-left: 50px;
  display: flex;
`

const LiContainer = styled.li`
  list-style-type: none;
`

const LinkContainer = styled.div`
  padding: 20px;
  &:hover { background-color: orange; }
`
const SpanContainer = styled.span`
  outline: none;
  display: block;
  color: white;
  text-align: center;
  text-decoration: none;
`

export const NavBar = () => {
  return (
    <NavContainer>
      <div>
        <UlContainer className="links">
          <LiContainer>
            <LinkContainer>
              <Link to="/sign-up">
                <SpanContainer>
                  Регистрация
                </SpanContainer>
              </Link>
            </LinkContainer>
          </LiContainer>
          <LiContainer>
          <LinkContainer>
            <Link to="/sign-in">
              <SpanContainer>
                Войти
              </SpanContainer>
            </Link>
          </LinkContainer>
          </LiContainer>
        </UlContainer>
      </div>
    </NavContainer>
  );
}
