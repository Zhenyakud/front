import { ComponentProps, FC } from "react";
import Card from "@material-ui/core/Card";
import { CardContent, IconButton, Typography } from "@material-ui/core";
import styled from "styled-components";
import DeleteIcon from '@material-ui/icons/Delete';
import { actions, useAppDispatch } from "../../Store";

const CardContainer = styled.div`
  margin: 8px;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: end;
`;

export interface ColumnProps extends ComponentProps<"div"> {
  columnId: string;
  cardId: string;
  text: string;
}

export const Cards: FC<ColumnProps> = ({ columnId, cardId, text }) => {

  const dispatch = useAppDispatch();

  const tryDeleteCard = async () => {
    await dispatch(actions.cards.deelteCard({ cardId, columnId }));
  }

  return (
    <CardContainer key={cardId}>
      <Card>
        <CardContent>
          <Typography gutterBottom>{text}</Typography>
        </CardContent>
        <ButtonContainer>
          <IconButton aria-label="delete" size="small" onClick={tryDeleteCard}>
            <DeleteIcon />
          </IconButton>
        </ButtonContainer>
      </Card>
    </CardContainer>
  );
}
