import { Button, TextField } from "@material-ui/core";
import { useEffect, useState } from "react";
import styled from "styled-components";
import { actions, selectors, useAppDispatch, useAppSelector } from "../../Store";
import { Columns } from "../Columns/Columns";

const BoardContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 100px; 
  margin-right: 10px;
  margin-left: 10px;
`;

const AddColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;
  margin-left: 8px;
  background-color: #dfe3e6;
  padding: 10px;
  border-radius: 3px;
`

const ButtonContainer = styled.div`
  height: 20px;
`

export const Board = () => {

  const [title, setTitle] = useState("");

  const tryCreateColumn = async () => {
    await dispatch(actions.columns.createColumn(title));
  }

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(actions.columns.getColumns());
  }, [])

  const { columns } = useAppSelector(selectors.columns.columns);

  const [isActive, setIsActive] = useState(false);

  const trySetIsActiveTrue = () => {
    setIsActive(true)
  };

  const trySetIsActiveFalse = () => {
    setIsActive(false)
  };

  const renderColumns = () => {
    return columns.map((column) => {
      return (
        <Columns
          key={column.id}
          columnId={column.id}
          cards={column.cards}
          title={column.title}
        />
      );
    });
  }

  return (
    <BoardContainer>
      {renderColumns()}
      {isActive ?
        <AddColumnContainer>
          <TextField
            id="outlined-basic"
            label="Название"
            variant="outlined"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
          <Button
            variant="contained"
            onClick={() => {
              trySetIsActiveFalse();
              tryCreateColumn();
            }}
          >
            Добавить колонку
          </Button>
        </AddColumnContainer>
        :
        <ButtonContainer>
          <Button
            variant="contained"
            onClick={trySetIsActiveTrue}
          >
            Создать колонку
          </Button>
        </ButtonContainer>
      }
    </BoardContainer >
  );
}
