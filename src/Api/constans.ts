export const API_URL = "https://backbac.herokuapp.com";

export const paths = {
  signUp: "/auth/sign-up",
  signIn: "/auth/sign-in",
  columns: "/columns",
  cards: "/cards",
};
