import axios, { AxiosRequestConfig } from "axios";
import { store } from "../../Store";
import { API_URL } from "../constans";

const config: AxiosRequestConfig = {
  baseURL: API_URL,
  headers: {
    "access-control-allow-origin": "*",
    "Content-Type": "application/json",
  },
};

export const httpClient = axios.create(config);

httpClient.interceptors.request.use((config) => {
  const token = store.getState().auth.accessToken;
  if (token) {
    config.headers!.Authorization = `Bearer ${token}`;
  }
  return config;
});
