export type SignInDto = {
  email: string,
  password: string,
}

export type SignInResponseDto = {
  userId: string,
  accessToken: string,
  refreshToken: string,
}

export type SignUpDto = {
  email: string,
  password: string,
}

export type SignUpResponseDto = {
  userId: string,
  accessToken: string,
  refreshToken: string,
}

export type CardResponseDto = {
  id: string;
  text: string;
  columnId: string;
}

export type CreateCardDto = {
  text: string;
  columnId: string;
}

export type DeleteCardDto = {
  cardId: string;
  columnId: string;
}

export type ColumnResponseDto = {
  id: string;
  title: string;
  userId: string;
  cards: CardResponseDto[];
}
