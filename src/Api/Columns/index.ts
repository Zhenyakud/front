import { httpClient } from "../Config";
import { paths } from "../constans";
import { ColumnResponseDto } from "../Types/types";

export const fetchColumns = async (): Promise<ColumnResponseDto[]> => {
  const res = await httpClient.get(paths.columns);
  return res.data;
}

export const uploadColumns = async (title: string): Promise<ColumnResponseDto> => {
  const { data } = await httpClient.post(paths.columns, { title });
  const column = data;
  column.cards = [];
  
  return column;
}

export const removeColumn = async (columnId: string): Promise<string> => {
  await httpClient.delete(paths.columns + '/' + columnId);
  return columnId;
}
