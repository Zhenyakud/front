import { httpClient } from "../Config";
import { paths } from "../constans";
import { CardResponseDto, CreateCardDto, DeleteCardDto } from "../Types/types";

export const fetchCards = async (columnId: CardResponseDto['columnId']): Promise<CardResponseDto[]> => {
  const res = await httpClient.get(paths.columns + '/' + columnId + '/cards');
  return res.data;
}

export const uploadCrard = async (card: CreateCardDto): Promise<CardResponseDto> => {
  const res = await httpClient.post(paths.cards, card);
  return res.data;
}

export const removeCard = async (data: DeleteCardDto): Promise<DeleteCardDto> => {
  await httpClient.delete(paths.cards + '/' + data.cardId);
  return data;
}
