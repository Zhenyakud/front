import { useEffect } from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import styled from "styled-components";
import { Board } from "../Components/Board/Board";
import { NavBar } from "../Components/NavBar/NavBar";
import { SignInForm } from "../Components/SignInDorm/SignInForm";
import { SignUpForm } from "../Components/SignUpForm/SignUpForm";
import { actions, selectors, useAppDispatch, useAppSelector } from "../Store";

const Container = styled.div`
  display: flex;
  flex-direction: column;
`

export const PagesRouter = () => {

  const { accessToken } = useAppSelector(selectors.auth.auth);

  

  return (
    <Container>
      <NavBar />
      <Routes>
        <Route path="/" element={ accessToken ? <Board /> : <SignInForm /> } />
        <Route path="/sign-up" element={ <SignUpForm /> }/>
        <Route path="/sign-in" element={ <SignInForm /> }/>
      </Routes>
    </Container>
  );
}
