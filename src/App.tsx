import { PagesRouter } from './Router/PagesRouter';

function App() {
  return (
    <PagesRouter/>
  );
}

export default App;
