import { createReducer, PayloadAction } from "@reduxjs/toolkit";
import { CardResponseDto, ColumnResponseDto } from "../../Api/Types/types";
import { cardsActions } from "./thunk";

export interface CardsState {
  cards: CardResponseDto[],
};

const initialState: CardsState = {
  cards: [],
};

export const cardsReducer = createReducer(
  initialState, 
  (builder) => {
    builder
      .addCase(
        cardsActions.getCards.fulfilled.type,
        (state, { payload }: PayloadAction<CardResponseDto[]>) => {
          state.cards.push(...payload);
        }
      )
  }
);
