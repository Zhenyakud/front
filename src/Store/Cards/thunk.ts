import { createAsyncThunk } from "@reduxjs/toolkit";
import { fetchCards, removeCard, uploadCrard } from "../../Api/Cards";
import { CardResponseDto, CreateCardDto, DeleteCardDto } from "../../Api/Types/types";

const getCards = createAsyncThunk<CardResponseDto[], string>(
  "getCards",
  async (columnsId) => {
    try {
      return await fetchCards(columnsId);
    } catch (e) {
      throw e;
    }
  }
);

const createCard = createAsyncThunk<CardResponseDto, CreateCardDto> (
  "createCard",
  async (data) => {
    try {
      return await uploadCrard(data);
    } catch (e) {
      throw e;
    }
  }
)

const deelteCard = createAsyncThunk<DeleteCardDto, DeleteCardDto> (
  "deleteCard",
  async (data) => {
    try {
      return await removeCard(data);
    } catch (e) {
      throw e;
    }
  }
);

export const cardsActions = {
  getCards,
  createCard,
  deelteCard,
};
