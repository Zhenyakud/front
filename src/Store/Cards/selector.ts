import { RootState } from "../Config";

export const selectors = {
  cards: (state: RootState) => state.cards,
  cardsByColumnId: (columnId: string) => (state: RootState) => { 
    return state.cards.cards.filter(card => card.columnId === columnId); 
  }
};
