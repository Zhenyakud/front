import { createAsyncThunk } from "@reduxjs/toolkit";
import { SignIn, SignUp } from "../../Api/Auth";
import { SignInDto, SignInResponseDto, SignUpDto, SignUpResponseDto } from "../../Api/Types/types";

const signUp = createAsyncThunk<SignUpResponseDto, SignUpDto>(
  "signUp",
  async (data) => {
    try {
      return await SignUp(data);
    } catch (e) {
      throw e;
    }
  }
);

const signIn = createAsyncThunk<SignInResponseDto, SignInDto>(
  "signIn",
  async (data) => {
    try {
      return await SignIn(data);
    } catch (e) {
      throw e;
    }
  }
);

export const authActions = {
  signUp,
  signIn,
};
