import { RootState } from "../Config";

export const selectors = {
  auth: (state: RootState) => state.auth,
};
