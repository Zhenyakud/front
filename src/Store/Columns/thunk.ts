import { createAsyncThunk } from "@reduxjs/toolkit";
import { fetchColumns, removeColumn, uploadColumns } from "../../Api/Columns";
import { ColumnResponseDto } from "../../Api/Types/types";

const getColumns = createAsyncThunk<ColumnResponseDto[]>(
  "getColumns",
  async () => {
    try {
      return await fetchColumns();
    } catch (e) {
      throw e;
    }
  }
);

const createColumn = createAsyncThunk<ColumnResponseDto, string>(
  "createColumn",
  async (data) => {
    try {
      return await uploadColumns(data);
    } catch (e) {
      throw e;
    }
  }
);

const deleteColumn = createAsyncThunk<string, string>(
  "deleteColumn",
  async (data) => {
    try {
      return await removeColumn(data);
    } catch (e) {
      throw e;
    }
  }
);

export const columnsActions = {
  getColumns,
  createColumn,
  deleteColumn,
};
