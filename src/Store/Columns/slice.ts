import { createReducer, PayloadAction } from "@reduxjs/toolkit";
import { CardResponseDto, ColumnResponseDto, DeleteCardDto } from "../../Api/Types/types";
import { cardsActions } from "../Cards";
import { columnsActions } from "./thunk";

export interface ColumnState {
  columns: ColumnResponseDto[],
};

const initialState: ColumnState = {
  columns: [],
};

export const columnsReducer = createReducer(
  initialState, 
  (builder) => {
    builder
      .addCase(
        columnsActions.getColumns.fulfilled.type,
        (state, { payload }: PayloadAction<ColumnResponseDto[]>) => {
          state.columns = payload;
        }
      )
      .addCase(
        columnsActions.createColumn.fulfilled.type,
        (state, { payload }: PayloadAction<ColumnResponseDto>) => {
          state.columns.push(payload);
        }
      )
      .addCase(
        columnsActions.deleteColumn.fulfilled.type,
        (state, payload: PayloadAction<string> ) => {
          state.columns = state.columns.filter((column) => column.id !== payload.payload);
        }
      )
      .addCase(
        cardsActions.createCard.fulfilled.type,
        (state, { payload }: PayloadAction<CardResponseDto> ) => {
          state.columns.map((column) => {
            if (column.id === payload.columnId) {
              column.cards.push(payload);
            }
          });
        }
      )
      .addCase(
        cardsActions.deelteCard.fulfilled.type,
        (state, { payload }: PayloadAction<DeleteCardDto> ) => {
          state.columns.map((column) => {
            if (column.id === payload.columnId) {
              column.cards = column.cards.filter((card) => card.id !== payload.cardId);
            }
          });
        }
      )
  }
);
