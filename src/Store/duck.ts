import * as auth from "./Auth";
import * as columns from "./Columns";
import * as cards from "./Cards";

export const actions = {
  auth: auth.authActions,
  columns: columns.columnsActions,
  cards: cards.cardsActions,
};

export const selectors = {
  auth: auth.selectors,
  columns: columns.selectors,
  cards: cards.selectors,
};
